import * as cp from 'child_process';
import * as path from 'path';
import * as fs from 'fs';
import semver from 'semver';
import os from 'os';
import { loadDotEnv } from './load-dot-env';

/**
 * use with care! 
 * Highly experimental!
 * only run once per branch!
 * only use if you know what you are doing!
 * Always check git changes before committing after using this script!
 */

/**
 * Usage - run from project root
 * check mode - are all affected projects version bumped?
 * ts-node ./tools/ci/scripts/patch-affected.ts check
 * write mode - writes next versions to all affected files
 * ts-node ./tools/ci/scripts/patch-affected.ts write
 */

/**
 * Script auto patches versions for all affected projects in workspace.
 * Bump version by hand if any other semver bump is needed than patch.
 */

interface project {
  name: string;
  path: string;
  type: 'app' | 'lib';
}

const getDirectories = (srcPath: string) => fs.readdirSync(srcPath).filter(file => fs.statSync(path.join(srcPath, file)).isDirectory());

(async () => {

  // console.log('starting');
  const [, , ...args] = process.argv;

  // only json file arg is required!
  // other args only used for debugging on local machine
  if (!args || !args[0]) {
    console.error(`
      ERROR expected argument:
      check
      ---or---
      write
    `);
    return;
  }

  if (!process.env.GITLAB_CI) {
    await loadDotEnv();
  }

  const mode = args[0] === 'write' ? 'write' : 'check';

  // get affected projects names
  const affected = cp.execSync(`nx print-affected base=origin/master --select=projects`).toString();

  // console.log(affected);

  // format to array and remove e2e test projects
  const affectedArray = affected.trim().split(', ').filter(e => !e.endsWith('-e2e'));

  const affectedProjects = readAffectedProjects(affectedArray);

  // console.log(affectedArray);

  // patch version / package file version field
  for (const project of affectedProjects) {
    // 'security-test' is not versioned
    if (project.name === 'security-test') continue;

    let fileContent;
    let newFile;
    try {
      fileContent = await fs.promises.readFile(`${project.path}`, 'utf-8');
      const vJson = JSON.parse(fileContent);
      const version = vJson.version;
      const newV = semver.inc(version, 'patch');
      vJson.version = newV;
      newFile = JSON.stringify(vJson, null, '  ').concat(os.EOL);

      const tagInfo = getTagInfoFromApi(project);
      let infoString = versionBumpWarn(tagInfo, version);

      console.log(`${project.type} | ${project.name}:`);
      console.log(`    | this ${version} - next ${newV}`);
      console.log(`    | ${tagInfo} ${infoString}`);
      console.log(os.EOL);
    } catch (error) {
      console.error(`ERROR: reading file at ${project.path}`);
      continue;
    }
    if (mode === 'write') {
      try {
        // const testPath = path.parse(app.path).dir + '/version-test.json';
        await fs.promises.writeFile(project.path, newFile, 'utf-8');
      } catch (error) {
        console.error(`ERROR: writing file for ${project.name}`);
      }
    }
  }
})();

function readAffectedProjects(affectedArray: string[]): project[] {
  // get project root
  const rootDir = path.join(__dirname, '../../..');

  console.log(`project root: \n`, rootDir);

  const appsDir = rootDir + '/apps';
  const libsDir = rootDir + '/libs';

  // get all folder in sub dirs - used to determine project type (app or lib)
  const appsDirs = getDirectories(appsDir);
  const libsDirs = getDirectories(libsDir);
  // console.log(appsDirs);

  // filter only affected by project type
  const affectedApps = appsDirs.filter(v => affectedArray.includes(v));
  const affectedLibs = libsDirs.filter(v => affectedArray.includes(v));

  console.log(`affected apps: \n`, affectedApps);
  console.log(`affected libs: \n`, affectedLibs);

  // create array with interface containing type and path
  const affectedProjects: project[] = [];
  affectedApps.map(appName => affectedProjects.push({
    name: appName,
    path: appsDir + '/' + appName + '/version.json',
    type: 'app',
  }));
  affectedLibs.map(libName => affectedProjects.push({
    name: libName,
    path: libsDir + '/' + libName + '/package.json',
    type: 'lib',
  }));

  return affectedProjects;
}

function versionBumpWarn(tagInfo: string, currentVersion: string): string {
  const tagVS = tagInfo.slice(tagInfo.search(' last tag '), tagInfo.length - 1);
  const tagVersion = semver.coerce(tagVS)
  // console.log('tagVersion', tagVersion?.version);

  const thisVersion = semver.parse(currentVersion);
  // console.log('thisVersion', thisVersion?.version);
  let infoString = ''
  if (thisVersion && tagVersion) {
    if (tagVersion?.patch + 1 === thisVersion?.patch) {
      infoString = '✅';
    }
    if (thisVersion?.patch !== tagVersion?.patch + 1) {
      infoString = `❌ ${os.EOL}    | --> Wanted ${tagVersion.major}.${tagVersion.minor}.${tagVersion.patch + 1} ?`;
    }
  }
  return infoString;
}

function getTagInfoFromApi(project: project): string {
  // request api
  const baseUrl = process.env.CI_SERVER_URL;
  const projectId = process.env.CI_PROJECT_ID;
  const scriptCall = `node ./tools/ci/scripts/out/check-version.js`

  const pathParam = project.type === 'app' ? `./apps/${project.name}/version.json` : `./libs/${project.name}/package.json`;

  const scriptExec = `${scriptCall} ${pathParam} ${projectId} ${baseUrl}`

  const versionResp = cp.execSync(scriptExec).toString();
  const tagInfo = versionResp.slice(versionResp.search('this '), versionResp.length).trim();
  // console.info(tagInfo);

  return tagInfo;
}