import * as https from 'https';
import * as http from 'http';

type headerType = 'AuthBearer' | 'gitlab-ci-token' | 'JOB-TOKEN' | 'PRIVATE-TOKEN';
interface HttpHeader { [x: string]: string };
interface RequestHeader { token: string, type: headerType };


/**
 * Do a request with options provided.
 * uses https to make request
 * @param {Object} options - http req options
 * @return {Promise} a promise of request
 */
function makeGetRequest(options: http.RequestOptions) {
  // console.debug('DEBUG: https req options ' + JSON.stringify(options));
  return new Promise((resolve, reject) => {
    const req = https.get(options, res => reqFuncBody(res, resolve));
    onReqError(req, reject);
    // req.on('error', (err) => {
    //   reject(err);
    // });
    // req.end();
  });
}

// uses http to make proxied request
function makeProxyGetRequest(options: http.RequestOptions) {
  // console.debug('DEBUG: http req options ' + JSON.stringify(options));
  return new Promise((resolve, reject) => {
    const req = http.get(options, res => reqFuncBody(res, resolve));
    onReqError(req, reject);
    // req.on('error', (err) => {
    //   reject(err);
    // });
    // req.end();
  });
}

function reqFuncBody(res: any, resolve: any) {
  res.setEncoding('utf8');
  let responseBody = '';

  res.on('data', (chunk: any) => {
    responseBody += chunk;
  });

  res.on('end', () => {
    // console.debug(`DEBUG: responseBody: ${JSON.stringify(responseBody)}`);
    resolve(JSON.parse(responseBody));
  });
};

const onReqError = (req: http.ClientRequest, reject: (reason?: any) => void) => {
  req.on('error', (err) => {
    reject(err);
  });
  req.end();
}


// for debugging on non public gitlab instances
function getAuthHeader(token: string, headerType?: headerType): HttpHeader {
  //if (process.env.GITLAB_CI) {
  if (headerType === 'JOB-TOKEN') {
    // header for ci - token created by pipeline
    return { 'JOB-TOKEN': token };
  } else if (headerType === 'gitlab-ci-token') {// header for ci - created by pipeline
    return { 'gitlab-ci-token': token };
  } else if (headerType === 'AuthBearer') {
    // create via gitlab jwt api
    return { 'Authorization': 'Bearer ' + token }
  } else if (headerType === 'PRIVATE-TOKEN') {
    // header for local tests - create token via webui
    return { 'PRIVATE-TOKEN': token };
  }
  return {};
}

async function httpRequest(
  instanceUrl: string,
  path: string,
  proxy?: string,
  header?: RequestHeader,
): Promise<unknown> {
  let authHeader: HttpHeader | undefined;
  if (header && header?.token) {
    authHeader = getAuthHeader(header.token, header.type);
  }

  console.debug(`request path: ${instanceUrl}${path}`);
  // console.debug(`request header:`, header);
  if (proxy) {
    const proxyUrl = new URL(proxy);
    const httpProxyOptions: http.RequestOptions = {
      hostname: proxyUrl.hostname,
      port: proxyUrl.port,
      path: instanceUrl + path,
      headers: authHeader
    }
    return await makeProxyGetRequest(httpProxyOptions);
  } else {
    const iUrl = new URL(instanceUrl);
    const httpOptions: http.RequestOptions = {
      hostname: iUrl.hostname,
      path: path,
      headers: authHeader
    }
    return await makeGetRequest(httpOptions);
  }
}

export { httpRequest, RequestHeader };