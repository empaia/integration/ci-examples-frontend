# CI Script files

compile folder with 
`npm run tsc --  -p ./tools/ci/scripts/tsconfig.tools.json`


watch & compile folder
`npm run tsc --  --watch -p ./tools/ci/scripts/tsconfig.tools.json`


run single output scripts from out folder with:
`node <./path/scriptname>`

 or watchmode: 
`nodemon <./path/scriptname>`


run ts directly with 
`ts-node <./path/scriptname>` 
or watchmode with 
`nodemon <./path/scriptname>`


CI pipeline should only use compiled js to avoid unnecessary dependency to ts-node.


# gitlab

ProjectID: 34540527
RepositoryID: 3026209

get images tag 
http GET https://gitlab.com/api/v4/projects/34540527/registry/repositories/3026209/tags?per_page=100

get image tags other possibility: 

http GET "https://gitlab.com/api/v4/registry/repositories/3026209?tags=true&per_page=100" 
