"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.printWarn = exports.printSuccess = exports.printInfo = exports.printError = void 0;
const os = require("os");
const RED_COLOR = '\x1b[31m';
const GREEN_COLOR = '\x1b[32m';
const YELLOW_COLOR = '\x1b[33m';
const RESET_COLOR = '\x1b[0m';
function printError(msg) {
    process.stderr.write(`${RED_COLOR}${msg}${RESET_COLOR}${os.EOL}`);
}
exports.printError = printError;
function printWarn(msg) {
    process.stdout.write(`${YELLOW_COLOR}${msg}${RESET_COLOR}${os.EOL}`);
}
exports.printWarn = printWarn;
function printSuccess(msg) {
    process.stdout.write(`${GREEN_COLOR}${msg}${RESET_COLOR}${os.EOL}`);
}
exports.printSuccess = printSuccess;
function printInfo(msg) {
    process.stdout.write(`${YELLOW_COLOR}${msg}${RESET_COLOR}${os.EOL}`);
}
exports.printInfo = printInfo;
