"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadDotEnv = void 0;
// import * as dotenv from 'dotenv';
// importing module 'dotenv' dynamically
// as it is only used for local dev we do not need this on the ci server
async function importModule(moduleName) {
    // console.log("importing ", moduleName);
    const importedModule = await Promise.resolve().then(() => require(moduleName));
    // console.log("\timported ...");
    return importedModule;
}
// const dotEnvPath = process.cwd() + '/tools/ci/scripts/';
// const dotEnvFile = '.env';
// const dotEnvSampleFile = 'sample.env';
// const createDotEnv = async () => {
//   // check if dot env exists
//   const dotEnvPathOs = path.parse(dotEnvPath);
//   const sampleEnv = dotEnvPathOs.dir +  path.sep + dotEnvSampleFile;
//   const dotEnv = dotEnvPathOs.dir +  path.sep + dotEnvFile;
//   if (!fs.existsSync(sampleEnv)) {
//     console.debug('.env not found! Creating...');
//     fs.copyFile(sampleEnv, dotEnv, (err) => {
//       if (err) { throw err };
//       console.log('sample.env copied to .env');
//     });
//   }
// }
const loadDotEnv = async () => {
    let moduleName = "dotenv";
    let dotenv = await importModule(moduleName);
    // console.log("importedModule", dotenv);
    const dotEnvPath = process.cwd() + '/tools/ci/scripts/';
    const dotEnvFile = '.ci.env';
    console.debug('.env path:', dotEnvPath + dotEnvFile);
    // await createDotEnv();
    const envRes = dotenv.config({ path: dotEnvPath + dotEnvFile });
    if (envRes.error) {
        throw envRes.error;
    }
};
exports.loadDotEnv = loadDotEnv;
// export const loadDotEnvExport = (process.env.GITLAB_CI)
//   ? null
//   : { loadDotEnv };
