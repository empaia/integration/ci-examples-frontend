"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cp = require("child_process");
const path = require("path");
const fs = require("fs");
const semver_1 = require("semver");
const os_1 = require("os");
const load_dot_env_1 = require("./load-dot-env");
const getDirectories = (srcPath) => fs.readdirSync(srcPath).filter(file => fs.statSync(path.join(srcPath, file)).isDirectory());
(async () => {
    // console.log('starting');
    const [, , ...args] = process.argv;
    // only json file arg is required!
    // other args only used for debugging on local machine
    if (!args || !args[0]) {
        console.error(`
      ERROR expected argument:
      check
      ---or---
      write
    `);
        return;
    }
    if (!process.env.GITLAB_CI) {
        await (0, load_dot_env_1.loadDotEnv)();
    }
    const mode = args[0] === 'write' ? 'write' : 'check';
    // get affected projects names
    const affected = cp.execSync(`nx print-affected base=origin/master --select=projects`).toString();
    // console.log(affected);
    // format to array and remove e2e test projects
    const affectedArray = affected.trim().split(', ').filter(e => !e.endsWith('-e2e'));
    const affectedProjects = readAffectedProjects(affectedArray);
    // console.log(affectedArray);
    // patch version / package file version field
    for (const project of affectedProjects) {
        // 'security-test' is not versioned
        if (project.name === 'security-test')
            continue;
        let fileContent;
        let newFile;
        try {
            fileContent = await fs.promises.readFile(`${project.path}`, 'utf-8');
            const vJson = JSON.parse(fileContent);
            const version = vJson.version;
            const newV = semver_1.default.inc(version, 'patch');
            vJson.version = newV;
            newFile = JSON.stringify(vJson, null, '  ').concat(os_1.default.EOL);
            const tagInfo = getTagInfoFromApi(project);
            let infoString = versionBumpWarn(tagInfo, version);
            console.log(`${project.type} | ${project.name}:`);
            console.log(`    | this ${version} - next ${newV}`);
            console.log(`    | ${tagInfo} ${infoString}`);
            console.log(os_1.default.EOL);
        }
        catch (error) {
            console.error(`ERROR: reading file at ${project.path}`);
            continue;
        }
        if (mode === 'write') {
            try {
                // const testPath = path.parse(app.path).dir + '/version-test.json';
                await fs.promises.writeFile(project.path, newFile, 'utf-8');
            }
            catch (error) {
                console.error(`ERROR: writing file for ${project.name}`);
            }
        }
    }
})();
function readAffectedProjects(affectedArray) {
    // get project root
    const rootDir = path.join(__dirname, '../../..');
    console.log(`project root: \n`, rootDir);
    const appsDir = rootDir + '/apps';
    const libsDir = rootDir + '/libs';
    // get all folder in sub dirs - used to determine project type (app or lib)
    const appsDirs = getDirectories(appsDir);
    const libsDirs = getDirectories(libsDir);
    // console.log(appsDirs);
    // filter only affected by project type
    const affectedApps = appsDirs.filter(v => affectedArray.includes(v));
    const affectedLibs = libsDirs.filter(v => affectedArray.includes(v));
    console.log(`affected apps: \n`, affectedApps);
    console.log(`affected libs: \n`, affectedLibs);
    // create array with interface containing type and path
    const affectedProjects = [];
    affectedApps.map(appName => affectedProjects.push({
        name: appName,
        path: appsDir + '/' + appName + '/version.json',
        type: 'app',
    }));
    affectedLibs.map(libName => affectedProjects.push({
        name: libName,
        path: libsDir + '/' + libName + '/package.json',
        type: 'lib',
    }));
    return affectedProjects;
}
function versionBumpWarn(tagInfo, currentVersion) {
    const tagVS = tagInfo.slice(tagInfo.search(' last tag '), tagInfo.length - 1);
    const tagVersion = semver_1.default.coerce(tagVS);
    // console.log('tagVersion', tagVersion?.version);
    const thisVersion = semver_1.default.parse(currentVersion);
    // console.log('thisVersion', thisVersion?.version);
    let infoString = '';
    if (thisVersion && tagVersion) {
        if (tagVersion?.patch + 1 === thisVersion?.patch) {
            infoString = '✅';
        }
        if (thisVersion?.patch !== tagVersion?.patch + 1) {
            infoString = `❌ ${os_1.default.EOL}    | --> Wanted ${tagVersion.major}.${tagVersion.minor}.${tagVersion.patch + 1} ?`;
        }
    }
    return infoString;
}
function getTagInfoFromApi(project) {
    // request api
    const baseUrl = process.env.CI_SERVER_URL;
    const projectId = process.env.CI_PROJECT_ID;
    const scriptCall = `node ./tools/ci/scripts/out/check-version.js`;
    const pathParam = project.type === 'app' ? `./apps/${project.name}/version.json` : `./libs/${project.name}/package.json`;
    const scriptExec = `${scriptCall} ${pathParam} ${projectId} ${baseUrl}`;
    const versionResp = cp.execSync(scriptExec).toString();
    const tagInfo = versionResp.slice(versionResp.search('this '), versionResp.length).trim();
    // console.info(tagInfo);
    return tagInfo;
}
