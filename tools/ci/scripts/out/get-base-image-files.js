"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getBaseImageName = exports.getBaseImageFiles = void 0;
const fs_1 = require("fs");
const path_1 = require("path");
const load_dot_env_1 = require("./load-dot-env");
/**
 * get all files in directory
 * @param dirPath directory to check
 * @param arrayOfFiles existing array with files - used for recursive call
 * @returns array of files in directory and all subdirs
 */
const getAllFiles = (dirPath, arrayOfFiles) => {
    const files = (0, fs_1.readdirSync)(dirPath);
    arrayOfFiles = arrayOfFiles || [];
    files.forEach(function (file) {
        if ((0, fs_1.statSync)(dirPath + "/" + file).isDirectory()) {
            arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles);
        }
        else {
            arrayOfFiles.push((0, path_1.join)(dirPath, "/", file));
        }
    });
    return arrayOfFiles;
};
const getDockerfiles = (rootDir) => {
    const dockerDir = rootDir + '/tools/docker';
    // get all Dockerfile
    const files = getAllFiles(dockerDir, []).filter(is => is.search('Dockerfile') > -1);
    return files;
};
const addRootCiFile = (rootDir, files) => {
    // add '/.gitlab-ci.yml to files
    const entries = [rootDir + '/.gitlab-ci.yml', ...files];
    return entries;
};
const getBaseImageFiles = () => {
    const rootDir = process.cwd();
    const files = getDockerfiles(rootDir);
    const allFiles = addRootCiFile(rootDir, files);
    return allFiles;
};
exports.getBaseImageFiles = getBaseImageFiles;
// construct image name from env vars
const getBaseImageName = async () => {
    if (!process.env.GITLAB_CI) {
        await (0, load_dot_env_1.loadDotEnv)();
    }
    // CI_REGISTRY = registry.gitlab.com
    // CI_PROJECT_PATH = empaia/integration/ci-examples-frontend
    const baseImageName = `${process.env.CI_REGISTRY}/${process.env.CI_PROJECT_PATH}/ci-base-image`;
    return baseImageName;
};
exports.getBaseImageName = getBaseImageName;
