"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getLockfileHash = void 0;
const fs = require("fs");
const crypto = require("crypto");
/**
 * normalizes line endings and
 * computes hash of package-lock.json
 * used to verify & test hash value on different os
 * should produce same output as running
 * $ sha256sum ./package-lock.json
 * on linux
 */
function getLockfileHash() {
    // compute sha256 of lockfile - value is used a cache key for base ci image
    const fileBuffer = fs.readFileSync('./package-lock.json', { encoding: 'utf-8' });
    const hashSum = crypto.createHash('sha256');
    hashSum.update(fileBuffer.replace(/\r\n/g, "\n"));
    const hashValHex = hashSum.digest('hex');
    // console.info('package-lock.json sha256 is:');
    // console.info(hashValHex);
    return hashValHex;
}
exports.getLockfileHash = getLockfileHash;
