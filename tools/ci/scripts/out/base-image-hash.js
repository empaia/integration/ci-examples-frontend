"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const os = require("os");
const get_base_image_files_1 = require("./get-base-image-files");
const get_lockfile_hash_1 = require("./get-lockfile-hash");
/**
 * main function
 * patch all files in file list with new version hash or
 * check if all files match specified version hash or
 * calculate current package-lock.json hash and do check / patch
 * script searches for baseImageName: and replaces docker version
 * new version must be passed as argument to this script
 * run this script from project root with
 * to patch new version:
 * $ ts-node ./tools/ci/scripts/base-image-hash.ts patch <sha256_hex_value>
 * to check version:
 * $ ts-node ./tools/ci/scripts/base-image-hash.ts check <sha256_hex_value>
 * to calculate workspace package-lock.json hash and patch it
 * $ ts-node ./tools/ci/scripts/base-image-hash.ts patch-current
 * to calculate workspace package-lock.json hash and check it
 * $ ts-node ./tools/ci/scripts/base-image-hash.ts check-current
 * @arg {string} hash - package-lock.json hash value
 */
(async () => {
    // parse arguments
    const [, , ...args] = process.argv;
    // default exit with error
    process.exitCode = 1;
    // check args
    if (!args || !args[0]) {
        console.error(`
      ERROR expected first argument mode: 
      check / patch / check-current / patch-current`);
        if (args[0] === 'patch' || args[0] === 'check' || !args[1]) {
            console.error(`
      ERROR expected second argument: 
      NEW-LOCKFILE-HASH`);
        }
        console.error(`
      Got:`, args);
        return;
    }
    let mode;
    const modeArg = args[0];
    if (modeArg === 'patch' ||
        modeArg === 'check' ||
        modeArg === 'patch-current' ||
        modeArg === 'check-current') {
        mode = modeArg;
    }
    else {
        return;
    }
    let versionHashArg = undefined;
    if (mode === 'check-current' || mode === 'patch-current') {
        // const scriptResult = cp.execSync(`node ${__dirname}/base-image-get-lockfile-hash.js`).toString();
        // versionHashArg = scriptResult.split(':').at(1)?.trim();
        console.info(`Using current lockfile hash!`);
        versionHashArg = (0, get_lockfile_hash_1.getLockfileHash)();
    }
    else if (args[1]) {
        // version to patch -or- version to check
        versionHashArg = args.at(1)?.trim();
    }
    else {
        console.error(`
    ERROR expected second argument: 
    NEW-LOCKFILE-HASH`);
    }
    // add '/.gitlab-ci.yml to files
    const entries = (0, get_base_image_files_1.getBaseImageFiles)();
    // console.log(entries);
    // use error count as exit code
    let errorCount = 0;
    const baseImageName = await (0, get_base_image_files_1.getBaseImageName)();
    for (const fileName of entries) {
        try {
            console.info(`${os.EOL}________________`);
            console.info(`processing file: ${fileName}`);
            // read single file
            const fileContent = await fs.promises.readFile(`${fileName}`, 'utf-8');
            // normalize line endings (CRLF to RF)
            const lines = fileContent.toString().replace(/\r\n/g, '\n').split('\n');
            // find baseImageName index in file
            const indexOfImageName = lines.map((line, index) => {
                if (line.search(baseImageName) > -1)
                    return index;
            }).filter(l => l).pop();
            // string not found abort file
            if (!indexOfImageName) {
                console.error('❌ ERROR: ci-base-image version string not found in:');
                console.error(fileName);
                errorCount += 1;
                continue;
            }
            ;
            // console.log(`indexofImageName Line ${indexOfImageName}`);
            // console.log(`line ${lines.at(indexOfImageName)}`);
            // extract line where image is defined
            const line = lines.at(indexOfImageName);
            if (line) {
                // split line into array at spaces
                const lineContent = line.split(' ');
                // find colon in base image name
                const indexOfVersion = lineContent.findIndex(i => i.split(':').find(is => is === baseImageName));
                const imageLineString = lineContent.at(indexOfVersion);
                // expects version hash after colon
                const currentVersion = imageLineString?.split(':')?.at(1)?.trim();
                if (mode === 'check' || mode === 'check-current') {
                    let logString = `Comparing hashes: 
            Version in file === Version to check
            ${currentVersion} === ${versionHashArg} 
          `;
                    if (currentVersion === versionHashArg) {
                        console.info(logString + '✅ Success: matches');
                    }
                    else {
                        console.info(logString + '❌ ERROR: no match');
                        errorCount += 1;
                    }
                }
                else if (mode === 'patch' || mode === 'patch-current') {
                    if (currentVersion === versionHashArg) {
                        console.info(`✅ skipping file - hashes up to date`);
                        continue;
                    }
                    else {
                        console.info(`old version hash`);
                        console.info(`${currentVersion}`);
                        console.info(`new version hash:`);
                        console.info(`${versionHashArg}`);
                    }
                    // console.info(`current base image:`)
                    // console.info(line);
                    // replace old version string with new one
                    lineContent.splice(indexOfVersion, 1, `${baseImageName}:${versionHashArg}`);
                    // join new line together
                    const newLine = lineContent.join(' ').trimEnd();
                    // console.info(`new base image:`)
                    // console.info(newLine);
                    // replace old line with new one in line array
                    lines.splice(indexOfImageName, 1, newLine);
                    // construct new file
                    const newFile = lines.join(os.EOL);
                    try {
                        // write new file to disk
                        await fs.promises.writeFile(fileName, newFile, 'utf-8');
                    }
                    catch (error) {
                        console.error(`ERROR: writing file ${fileName}`);
                    }
                }
            }
            // console.log(newFile);
        }
        catch (error) {
            console.error(`ERROR: reading file at ${fileName}`);
        }
    }
    process.exitCode = errorCount;
    return;
})();
