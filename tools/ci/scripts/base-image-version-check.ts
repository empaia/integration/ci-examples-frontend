import * as fs from 'fs';
import * as os from 'os';
import { getBaseImageFiles, getBaseImageName } from './get-base-image-files';
import { getLockfileHash } from './get-lockfile-hash';

/**
 * script compares ci-base-image version with package-lock.json sha256 sum
 */

/**
 * base image name & string to search for in files
 * modify if repo address changes or if use in new repo!
 */
// const baseImageName: string = 'registry.gitlab.com/empaia/integration/ci-examples-frontend/ci-base-image';


/**
 * get all Dockerfiles in /tools/docker
 * get gitlab-ci.yml
 * extract ci-base-image:version string
 * compare with current package-lock.json hash
 * if all version hashes match exit 0 success
 * else exit with error count
 */
(async () => {

  // default exit with error
  process.exitCode = 1;

  const entries = getBaseImageFiles();
  console.log('entries');
  console.log(entries);

  const hash = getLockfileHash();
  console.info('package-lock.json sha256 is:');
  console.info(hash);

  const baseImageName = await getBaseImageName();

  // use error count as exit code
  let errorCount = 0;

  for (const fileName of entries) {
    try {
      console.info(`${os.EOL}________________`);
      console.info(`processing file:`);
      console.info(`${fileName}`)
      // read single file
      const fileContent = await fs.promises.readFile(`${fileName}`, 'utf-8');
      // normalize line endings (CRLF to RF)
      const lines = fileContent.toString().replace(/\r\n/g, '\n').split('\n');
      // find baseImageName index in file
      const indexOfImageName = lines.map((line, index) => {
        if (line.search(baseImageName) > -1) return index;
      }).filter(l => l).pop();

      // string not found abort processing file
      if (!indexOfImageName) {
        console.error('❌ ERROR: ci-base-image version string not found in:');
        console.error(fileName);
        errorCount += 1;
        continue;
      };

      // extract line where image is defined
      const line = lines.at(indexOfImageName)

      if (line) {
        // split line into array at spaces
        const lineContent = line.split(' ');

        // find colon in base image name
        const indexOfVersion = lineContent.findIndex(i => i.split(':').find(is => is === baseImageName));
        const imageLineString = lineContent.at(indexOfVersion);

        // expects version hash after colon
        const currentVersion = imageLineString?.split(':')?.at(1)?.trim();

        const logString = `Comparing hashes: 
            Version in file === Version to check
            ${currentVersion} === ${hash} 
          `
        if (currentVersion === hash) {
          console.info(logString + '✅ Success: matches');
        } else {
          console.info(logString + '❌ ERROR: no match');
          errorCount += 1;
        }

      }
    } catch (error) {
      console.error(`ERROR: reading file at ${fileName}`);
    }
  }
  process.exitCode = errorCount;
  return;
})();
