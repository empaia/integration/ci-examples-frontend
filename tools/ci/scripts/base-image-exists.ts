
import { httpRequest, RequestHeader } from './http-request';
import { loadDotEnv } from './load-dot-env';


async function getToken(
  instanceUrl: string,
  projectPath: string,
  proxy: string | undefined
): Promise<unknown> {

  // requesting bearer token
  // const reqPath: string = `/jwt/auth?client_id=docker&offline_token=true&service=container_registry&scope=repository%3Aempaia%2Fintegration%2Fci-examples-frontend%2Fci-base-image%3Apull`
  const imageName: string = 'ci-base-image'
  const reqPath: string = `/jwt/auth?client_id=docker&offline_token=true&service=container_registry&scope=repository:${projectPath}/${imageName}:pull`

  return httpRequest(instanceUrl, reqPath, proxy);
}

async function getTags(
  instanceUrl: string,
  proxy: string | undefined,
  token: string
): Promise<unknown> {

  // // requesting docker tags
  // instanceUrl = 'https://registry.gitlab.com';
  const reqPath: string = `/v2/empaia/integration/ci-examples-frontend/ci-base-image/tags/list`

  const authHeader: RequestHeader = { token: token, type: 'AuthBearer' }
  return httpRequest(instanceUrl, reqPath, proxy, authHeader);
}

// as returned from gitlab docker api
interface DockerTags {
  name: string
  tags: string[]
};

// as returned from gitlab jwt auth api
interface Token {
  token: string
};

/**
 * check if a give version exists in gitlab docker registry 
 * compile with: npm run tsc -- -p /tools/ci/scripts/tsconfig.ci-scripts.json
 * run with: node ./tools/ci/scripts/out/base-image-check-registry.js
 */
(async () => {

  // default: exit with error
  process.exitCode = 1;

  // console.log('starting');
  const [, , ...args] = process.argv;

  if (!args || !args.at(0)) {
    console.error(`
      ERROR expected arguments:
      version-tag (required) - check if this image exists
    `);
    return;
  }

  const versionTag = args.at(0);

  if (!versionTag) {
    return;
  }

  if (!process.env.GITLAB_CI) {
    await loadDotEnv();
  }
  console.debug('process.env.CI_SERVER_URL', process.env.CI_SERVER_URL);

  // expects valid url - i.e. https://gitlab.com
  const gitlabInstanceUrl = process.env.CI_SERVER_URL;
  // env is registry.gitlab.com - prepend https
  const gitlabRegistryUrl = 'https://' + process.env.CI_REGISTRY;

  // repo path/name
  const projectPath = process.env.CI_PROJECT_PATH;

  // optional proxy settings
  const proxy = process.env.HTTP_PROXY || undefined;

  if (gitlabInstanceUrl && gitlabRegistryUrl && projectPath) {

    const tokenResp = await getToken(gitlabInstanceUrl, projectPath, proxy) as Token;
    // console.debug('token:', tokenResp);

    if (tokenResp.token) {
      const tagsResp = await getTags(gitlabRegistryUrl, proxy, tokenResp.token) as DockerTags;

      if (tagsResp.tags) {
        const set = new Set(tagsResp.tags);

        console.debug(set);

        const hasTag = set.has(versionTag);
        // console.debug(hasTag);
        const resultString = `${(hasTag ? '✅' : '❌')} Version tag${hasTag ? '' : ' not'} found in registry`;
        console.info(resultString);
        process.exitCode = hasTag ? 0 : 1;
        return;
      }
    }
  }
})();
