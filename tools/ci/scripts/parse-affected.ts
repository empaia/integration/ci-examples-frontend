#!/usr/bin/env ts-node
import * as os from 'os';

async function read(stream: any) {
  const chunks = [];
  for await (const chunk of stream) chunks.push(chunk);
  return Buffer.concat(chunks).toString('utf8');
}

/**
 * formats output of 'npm run nx print-affected --select=projects' 
 * as env vars.
 * i.e. if workbench client is in affected list env var
 * WORKBENCH_CLIENT=workbench-client 
 * is set
 */
(async () => {
  // console.log('starting');

  const input = await read(process.stdin);
  // console.log(`### input ${input}`);

  // split at line break -> remove undefined -> remove lines starting with '>'
  const parsed = input.split(/\r?\n/).filter(e => e).filter(l => !l.startsWith('>'));

  // anything left to process?
  if (!parsed || parsed.length === 0) { return; }

  // split at comma delimiter
  const parsedArray = parsed.at(0)?.split(', ');

  // console.info(`parsedArray ${JSON.stringify(parsedArray)} - ${parsedArray.length}`);

  if (parsedArray) {
    let env_var = '';
    for (const line of parsedArray) {
      if (line) {
        // convert to env var format
        env_var += `${line.toUpperCase().trim().split('-').join('_')}=${line}${os.EOL}`;
      }
    }
    // console.log(`### parsed output:`);
    process.stdout.write(env_var);
    // console.log('finished');}
  }
})();
