# build stage
FROM registry.gitlab.com/empaia/integration/ci-examples-frontend/ci-base-image:4a359e4742bf5e804fe3f80d617d60da627b1817da934add96305931ea31de62   AS build

WORKDIR /usr/angular-build
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json .
COPY decorate-angular-cli.js .

# symlink from base image node_modules to project folder
RUN ln -s ${NPM_MODULES_FOLDER} ./node_modules


# install dependencies
# RUN npm ci --cache ${NPM_CACHE_FOLDER} --prefer-offline

# build in production mode
COPY nx.json nx.json
COPY tsconfig.base.json tsconfig.base.json
COPY tools/ tools/
COPY libs/ libs/
COPY apps/ apps/

RUN npm run nx build workbench-client

# run stage
FROM nginx:alpine AS run

# Remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*

# copy configuration
COPY tools/docker/nginx.conf /etc/nginx/nginx.conf
COPY tools/docker/workbench-client/run.sh run.sh

# copy compiled angular sources from build stage
COPY --from=build /usr/angular-build/dist/apps/workbench-client /usr/share/nginx/html

ENTRYPOINT ["sh", "run.sh"]
