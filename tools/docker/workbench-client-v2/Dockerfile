# build stage
FROM registry.gitlab.com/empaia/integration/ci-examples-frontend/ci-base-image:4a359e4742bf5e804fe3f80d617d60da627b1817da934add96305931ea31de62   AS build


WORKDIR /usr/angular-build
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json .
COPY decorate-angular-cli.js .

# symlink from base image node_modules to project folder
RUN ln -s ${NPM_MODULES_FOLDER} ./node_modules

# build in production mode
COPY tools/docker/workbench-client-v2/angular.json angular.json
COPY nx.json nx.json
COPY tsconfig.base.json tsconfig.base.json
COPY libs/slide-viewer libs/slide-viewer
COPY libs/app-integration libs/app-integration
COPY apps/workbench-client-v2 apps/workbench-client-v2
RUN npm run nx build workbench-client-v2

# run stage
FROM nginx:alpine AS run

# Remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*

# copy configuration
COPY tools/docker/nginx.conf /etc/nginx/nginx.conf
COPY tools/docker/workbench-client-v2/run.sh run.sh

# copy compiled angular sources from build stage
COPY --from=build /usr/angular-build/dist/apps/workbench-client-v2 /usr/share/nginx/html

ENTRYPOINT ["sh", "run.sh"]
