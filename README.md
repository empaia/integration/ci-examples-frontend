

# CiExamplesFrontend 🌟🌟🌟

get-affected will fail on already merged branch!
1. locally work on feature branch
2. push
3. merge to main in web ui after successful pipeline 
4. push more changes to feature branch


## Build ci base image locally

in project root:

1. get current lockfile hash

  ts-node .\tools\ci\scripts\base-image-get-lockfile-hash.ts

2. to use lockfile hash as build tag set image name in docker-compose file
3. build new image with 

  docker-compose  -f .\tools\ci\base-image\docker-compose.yml build

4. push image with
  
  docker push registry.gitlab.com/empaia/integration/ci-examples-frontend/ci-base-image:<hash>
 
  > docker push registry.gitlab.com/empaia/integration/ci-examples-frontend/ci-base-image:latest

5. update references to base image with 

  ts-node .\tools\ci\scripts\base-image-hash.ts <hash>

6. wait for docker push to finish before pushing git changes!

7. optional: run patch affected script to bump versions
  ts-node .\tools\ci\scripts\patch-affected.ts
  
### Debug:

#### open shell in container

docker run -it registry.gitlab.com/empaia/integration/ci-examples-frontend/ci-base-image /bin/bash

#### git clone

apt update ; apt-get install apt-transport-https ca-certificates -y ; update-ca-certificates

git clone https://gitlab.com/empaia/integration/ci-examples-frontend.git
npm ci --cache /root/.cache/npm --prefer-offline --no-audit

#### check sha
 sha256sum root/lockfile/package-lock.json
 cat root/lockfile/package-lock.json.sha256
  
## print affected command  
  
nx print-affected --select=projects | node .\tools\ci\js-out\parse-affected.js



## Git Hooks

are installed via npm postinstall script

 `git config --local core.hookspath`

sets  `core.hookspath=./tools/ci/git-hooks`

should point to git hooks directory: 

`./tools/ci/git-hooks`


### pre commit hook

1. compiles ci scripts
2. patches current package-lock hash as docker base image version

✨