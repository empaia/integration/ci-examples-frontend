import { Component } from '@angular/core';

@Component({
  selector: 'ci-examples-frontend-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'workbench-client';

  bar() {
    // let foo = 0;
  }
}
