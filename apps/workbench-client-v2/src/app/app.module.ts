import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NxWelcomeComponent } from './nx-welcome.component';
import { RouterModule } from '@angular/router';

import { SlideViewerModule } from '@ci-examples-frontend/slide-viewer';
import { VendorAppCommunicationInterfaceModule } from '@ci-examples-frontend/vendor-app-communication-interface';

@NgModule({
  declarations: [
    AppComponent,
    NxWelcomeComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([], { initialNavigation: 'enabledBlocking' }),
    SlideViewerModule,
    VendorAppCommunicationInterfaceModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
